package net.tncy.aag.validator;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestISBNValidator {
    private static ISBNValidator validator;

    @BeforeClass
    public static void setUpClass() throws Exception {
        validator = new ISBNValidator();
    //    validator.initialize(ISBNFormat.ISBN_10, false);
    }

    @Test
    public void testValid() throws Exception {
        isValid("2266");
    }
    @Test
    public void testInvalidLength() throws Exception {
        isNotValid(""); // Fails on length verification (too short)
    }
    @Test
    public void testInvalidCharac() throws Exception {
        isNotValid("12!"); // Fails on length verification (too short)
    }
    @Test
    public void testInvalidBigLength() throws Exception {
        isNotValid("12345678901234567890123"); // Fails on length verification (too short)
    }

    @Test
    public void testInvalidBookLand() throws Exception {
        isNotValid("9782"); // Fails on length verification (too short)
    }
    private void isValid(String s) {
        assertTrue("Expected a valid ISBN.", validator.isValid(s, null));
    }

    private void isNotValid(String s) {
        assertFalse("Expected a invalid ISBN.", validator.isValid(s, null));
    }
}
