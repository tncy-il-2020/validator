package net.tncy.aag.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ISBNValidator implements ConstraintValidator<ISBN, String> {

    @Override
    public void initialize(ISBN constraintAnnotation) {
        // Ici le validateur peut accéder aux attribut de l’annotation.

    }

    @Override
    public boolean isValid(String bookNumber, ConstraintValidatorContext constraintContext) {
        boolean valid = bookNumber.length() > 0;
        // Algorithme de validation du numéro ISBN

        if (bookNumber.contains("!")) {
            valid = false;
        }
        if (bookNumber.length() > 20) {
            valid = false;
        }
        if (bookNumber.length() > 0) {
            if (bookNumber.substring(0, 3).equals("978")) {
                valid = false;
            }
        }

        return valid;

    }
}
